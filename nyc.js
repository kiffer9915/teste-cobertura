module.exports = {
    extends: '@istanbuljs/nyc-config-typescript',
    all: true,
    reportDir: './coverage',
    reporter: ['text', 'html'],
    exclude: ['**/*.spec.js', '**/*.spec.ts']
  };
  