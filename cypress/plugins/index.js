const instrument = require('istanbul-lib-instrument');


const { createInstrumenter } = require('istanbul-lib-instrument');
const babelCore = require('@babel/core');

module.exports = (on, config) => {
  on('file:preprocessor', (file) => {
    if (file.filePath.includes('node_modules')) {
      return;
    }
    const instrumenter = createInstrumenter();
    const output = babelCore.transform(file.source, {
      filename: file.filePath,
      plugins: [[instrumenter, { useInlineSourceMaps: false }]],
      presets: ['@babel/preset-env']
    });
    return output.code;
  });
};
